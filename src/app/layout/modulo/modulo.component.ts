import { Component, OnInit, ViewChild } from '@angular/core';
import { ModuloService } from './service/modulo.service';
import { Modulo } from '../../shared/modelo/seguridad/modulo';
import { HttpClient } from 'selenium-webdriver/http';
import { environment } from 'src/environments/environment';
import { QueryOptions } from './model/QueryOptions';
import { Table } from 'primeng/table';

@Component({
    selector: 'app-modulo',
    templateUrl: './modulo.component.html',
    styleUrls: ['./modulo.component.scss']
})
export class ModuloComponent implements OnInit {
    constructor(private moduloService: ModuloService) {
        this.cols = [{ field: 'descripcion', header: 'Descripción' }, { field: 'nombre', header: 'Nombre' }];
    }
    modulo: Modulo;
    modulos: Modulo[] = [];
    moduloSeleccionado: Modulo;
    displayDialog;
    loading = true;
    totalrecords;
    numerofilas = 10;
    pagina = 0;
    cols = [];

    ngOnInit() {
        // método para cargar los módulos paginados
        this.recuperarModulos();
    }
    recuperarModulos() {
        const q: QueryOptions = new QueryOptions();
        q.pageSize = this.numerofilas;
        q.pageNumber = this.pagina;
        this.moduloService.list(q).subscribe(resp => {
            this.modulos = resp.content;
            this.totalrecords = resp.totalElements;
        });
    }
    columnFilter(event: any) {
        console.log(event);
        console.log();
    }
    loadCarsLazy(event) {
        this.loading = true;

        // in a real application, make a remote request to load data using state metadata from event
        // event.first = First row offset
        // event.rows = Number of rows per page
        // event.sortField = Field name to sort with
        // event.sortOrder = Sort order as number, 1 for asc and -1 for dec
        // filters: FilterMetadata object having field as key and filter value, filter matchMode as value

        setTimeout(() => {
            if (this.modulos) {
                const q: QueryOptions = new QueryOptions();
                q.pageSize = event.rows;
                // Calculo de pagina
                let page = Math.ceil(event.first / this.numerofilas);
                this.pagina = page;
                if (page < 0) {
                    page = 0;
                }
                q.pageNumber = page;
                // Sorts
                q.sortField = event.sortField;
                q.sortOrder = event.sortOrder;
                console.log(event);
                if (event.filters.global) {
                    this.moduloService.find(q, event.filters.global.value).subscribe(resp => {
                        // console.log(resp);
                        this.modulos = resp.content;
                        this.totalrecords = resp.totalElements;
                    });
                } else {
                    this.moduloService.list(q).subscribe(resp => {
                        // console.log(resp);
                        this.modulos = resp.content;
                        this.totalrecords = resp.totalElements;
                    });
                }

                this.loading = false;
            }
        }, 1000);
    }
    showDialogToAdd() {
        this.modulo = {};
        this.displayDialog = true;
    }
    showDialogToEdit() {
        this.modulo = this.moduloSeleccionado;
        this.displayDialog = true;
    }
    onRowSelect(e) {
        // console.log(this.moduloSeleccionado);
    }
    guardar() {
        this.moduloService.create(this.modulo).subscribe(resp => {
            // console.log(resp);
            this.displayDialog = false;
            this.recuperarModulos();
        });
    }
    borrar() {
        this.moduloService.delete(this.moduloSeleccionado.idModulo).subscribe(resp => {
            console.log('Ok');
            this.moduloSeleccionado = null;
            this.recuperarModulos();
        });
    }
}
