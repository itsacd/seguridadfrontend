import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModuloComponent } from './modulo.component';
import { ModuloRoutingModule } from './modulo-routing.module';
import { PageHeaderModule } from 'src/app/shared';
import { TableModule } from 'primeng/table';
import {ButtonModule} from 'primeng/button';
import { ModuloService } from './service/modulo.service';
import { GenericService } from './service/generic.service';
import {DialogModule} from 'primeng/dialog';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule, ModuloRoutingModule, PageHeaderModule, TableModule, ButtonModule,
    DialogModule, FormsModule

  ],
  declarations: [ModuloComponent, ModuloComponent],
  providers: [GenericService, ModuloService]

})
export class ModuloModule { }
