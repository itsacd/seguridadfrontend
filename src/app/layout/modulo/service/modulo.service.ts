import { HttpClient } from '@angular/common/http';
import { Modulo } from '../../../shared/modelo/seguridad/modulo';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
@Injectable()
export class ModuloService extends GenericService<Modulo> {
  constructor(httpClient: HttpClient) {
    super(
      httpClient,
      environment.URL_AUTH,
      'modulo'
      );
  }
}
