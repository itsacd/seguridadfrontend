import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { LoginServiceService } from './service/login-service.service';
import { MessageService } from 'primeng/components/common/messageservice';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    constructor(public router: Router,
         private loginservice: LoginServiceService,
         private messageService: MessageService) {}
    usuario = {usuario: null, pass: null};
    ngOnInit() {}
    authentica() {
        this.loginservice.autentica(this.usuario).subscribe(resp => {
            console.log(resp);
            if (resp) {
            if (resp.valido === true) {
                localStorage.setItem('isLoggedin', 'true');
                this.messageService.add({severity: 'error', summary: 'Servicio de Autenticación', detail: 'Logueo correcto'});

                this.router.navigate(['/']);
            } else {
                this.messageService.add({severity: 'error', summary: 'Servicio de Autenticación', detail: 'Usuario o contraseña incorrecta'});

            }
            } else {
                this.messageService.add({severity: 'error', summary: 'Servicio de Autenticación', detail: 'Usuario o contraseña incorrecta'});

            }
        });
    }
}
