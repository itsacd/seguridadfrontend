import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class LoginServiceService {
    url = 'auth/';
    constructor(private http: HttpClient) {}
    autentica(usuario): Observable<any> {
        return this.http.post(environment.URL_AUTH + '/' + this.url, usuario);
    }
}
